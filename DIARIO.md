# Meu diário de bordo, um acompanhamento do que tenho estudado <!-- omit in toc -->

- [14/10/2019](#14102019)
- [30/09/2019](#30092019)
- [28/09/2019](#28092019)
- [26/09/2019](#26092019)
- [25/09/2019](#25092019)
- [24/09/2019](#24092019)
- [23/09/2019](#23092019)
- [22/09/2019](#22092019)
- [19/09/2019](#19092019)
- [18/09/2019](#18092019)
- [16/09/2019](#16092019)
- [12/09/2019](#12092019)
- [11/09/2019](#11092019)
- [10/09/2019](#10092019)
- [09/09/2019](#09092019)

---

## 14/10/2019

- Laboratórios 1 e 2 do capitulo 1 do curso 'Segurança e Alta Disponibilidade de Dados' na Data Science Academy

## 30/09/2019

- LPI Essentials
  - [Tópico 4.1_01: O sistema operacional Linux | Escolhendo um sistema operacional](https://learning.lpi.org/en/learning-materials/010-160/4/4.1/4.1_01/)

## 28/09/2019

- LPI Essentials
  - [Tópico 3.3_02: O poder da linha de comando | Transformando comandos em um script](https://learning.lpi.org/en/learning-materials/010-160/3/3.3/3.3_02/)
  
## 26/09/2019

- Transferência de arquivos do Mainframe para o Hadoop
  - [Como obter os dados do seu mainframe para análises do Hadoop](https://www.infoworld.com/article/3089852/how-to-get-your-mainframes-data-for-hadoop-analytics.html)
  - [Aprenda Sqoop Import Mainframe Tool - Sintaxe e Exemplos](https://data-flair.training/blogs/sqoop-import-mainframe/)
  - [Descarregando dados de mainframe no Hadoop](https://community.cloudera.com/t5/Community-Articles/Offloading-Mainframe-Data-into-Hadoop/ta-p/246129)
  - [Descarregando dados de mainframe no Hadoop: 4 coisas que você precisa saber](https://bigstep.com/blog/offloading-mainframe-data-hadoop-4-things-need-know)

## 25/09/2019

- HBASE - Design e Arquitetura
  - [Uma visão aprofundada da arquitetura do HBase](https://mapr.com/blog/in-depth-look-hbase-architecture/)
  - [Diretrizes para o design do esquema HBase](https://mapr.com/blog/guidelines-hbase-schema-design/)
  - [Banco de dados HBase e MapR: projetado para distribuição, escala e velocidade](https://mapr.com/blog/hbase-and-mapr-db-designed-distribution-scale-and-speed/)
  - [Como usar SQL, Hadoop, Drill, REST, JSON, NoSQL e HBase em um cliente REST simples](https://mapr.com/blog/how-use-sql-hadoop-drill-rest-json-nosql-and-hbase-simple-rest-client/)
  - [MapR Academy - Cursos sobre HBase](https://mapr.com/training/courses/)

## 24/09/2019

- LPI Essentials
  - [Tópico 3.3_01: O poder da linha de comando | Transformando comandos em um script](https://learning.lpi.org/en/learning-materials/010-160/3/3.3/3.3_01/)
  - [Tópico 3.2_02: O poder da linha de comando | Pesquisando e extraindo dados de arquivos](https://learning.lpi.org/en/learning-materials/010-160/3/3.2/3.2_02/)
  - [Tópico 3.2_01: O poder da linha de comando | Pesquisando e extraindo dados de arquivos](https://learning.lpi.org/en/learning-materials/010-160/3/3.2/3.2_01/)

## 23/09/2019

- LPI Essentials
  - [Tópico 3.1_01: O poder da linha de comando | Arquivando arquivos na linha de comando](https://learning.lpi.org/en/learning-materials/010-160/3/3.1/3.1_01/)
  - [Tópico 2.4_01: Encontrando seu caminho em um sistema Linux | Criando, movendo e excluindo arquivos](https://learning.lpi.org/en/learning-materials/010-160/2/2.4/2.4_01/)
  - [Tópico 2.3_02: Encontrando seu caminho em um sistema Linux | Usando listagem de diretórios e arquivos](https://learning.lpi.org/en/learning-materials/010-160/2/2.3/2.3_02/)

## 22/09/2019

- Leitura de artigos diversos e alguns vídeos

## 19/09/2019

- LPI Essentials
  - [Tópico 2.3_01: Encontrando seu caminho em um sistema Linux | Usando listagem de diretórios e arquivos](https://learning.lpi.org/en/learning-materials/010-160/2/2.3/2.3_01/)
  - [Tópico 2.3_02: Encontrando seu caminho em um sistema Linux | Usando listagem de diretórios e arquivos](https://learning.lpi.org/en/learning-materials/010-160/2/2.3/2.3_02/)
- Relação entre as políticas do Ranger e as permissões do HDFS
  - [Práticas recomendadas na autorização de HDFS com o Apache Ranger](https://blog.cloudera.com/best-practices-in-hdfs-authorization-with-apache-ranger/)
  - [Discussão sobre políticas do Ranger para HDFS](https://pierrevillard.com/2018/08/14/discussion-around-ranger-policies-for-hdfs/)
  - [Umask - Wikipedia](https://pt.wikipedia.org/wiki/Umask)
- Prática no Duolingo

## 18/09/2019

- Hoje participei do [AWS Game Day](https://aws.amazon.com/pt/gameday/) foi uma experiência bem interessante, pelo menos para mim, que até então não tinha utilizado a Console da AWS. Resolver situações da vida real realmente é uma forma eficiente de fixar os conhecimentos, recomendo!
- Leitura de artigos diversos;
- Prática no Duolingo;

## 16/09/2019

- Preparando o notebook para o AWS GameDay em 18/09/2019;
- Cadastrando no Google Cloud Partner Advantage;

## 12/09/2019

- Leitura de material sobre clientes Git e visualizadores de markdown para Android;
- LPI Essentials
  - [Tópico 2.2_01: Encontrando seu caminho em um sistema Linux | Usando a linha de comando para obter ajuda](https://learning.lpi.org/en/learning-materials/010-160/2/2.2/2.2_01/)

## 11/09/2019

- LPI Essentials
  - [Tópico 2.1_01: Encontrando seu caminho em um sistema Linux | Noções básicas de linha de comando](https://learning.lpi.org/en/learning-materials/010-160/2/2.1/2.1_01/)
  - [Tópico 2.1_02: A comunidade Linux e uma carreira em código aberto | Noções básicas de linha de comando](https://learning.lpi.org/en/learning-materials/010-160/2/2.1/2.1_02/)

## 10/09/2019

- Leitura de artigos diversos

## 09/09/2019

- LPI Essentials - Tópico 1.4_01:
  - [A comunidade Linux e uma carreira em código aberto | Habilidades em TIC e trabalho no Linux](https://learning.lpi.org/en/learning-materials/010-160/1/1.4/1.4_01/)
