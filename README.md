# Objetivo <!-- omit in toc -->

Organizar os materiais, documentações relevantes, [manter uma memória do que foi estudado](DIARIO.md), registrar demais informações úteis, documentação, artigos e reportagens interessantes.

## Estratégia de Estudos <!-- omit in toc -->

1. Estudar o conteúdo das certificações LPI (Essential e LPIC-1);
2. Diariamente estudar Inglês no Duolingo;
3. Ao menos uma vez por mês aplicar algum tutorial e/ou passo-a-passo para ***praticar*** e gerar novas dúvidas;

## Sumário <!-- omit in toc -->

- [Artigos, Reportagens e Documentação](#artigos-reportagens-e-documenta%c3%a7%c3%a3o)
  - [Geral](#geral)
  - [Linux](#linux)
  - [LPI Learning](#lpi-learning)
    - [Essentials](#essentials)
    - [LPIC-1](#lpic-1)
  - [Segurança](#seguran%c3%a7a)
  - [Git](#git)
  - [Markdown &amp; Mermaid](#markdown-amp-mermaid)
  - [Ansible](#ansible)
  - [Hadoop](#hadoop)
    - [Sqoop &amp; Importação de dados](#sqoop-amp-importa%c3%a7%c3%a3o-de-dados)
    - [Ranger](#ranger)
    - [HBase](#hbase)
  - [DevOps](#devops)
  - [Docker](#docker)
  - [Kubernetes](#kubernetes)
  - [Empresas](#empresas)
    - [AWS](#aws)
    - [HashiCorp](#hashicorp)
    - [IBM](#ibm)
    - [Mozilla](#mozilla)
  - [Sites](#sites)
  - [Ferramentas](#ferramentas)
- [Tutoriais e Passo-a-Passo](#tutoriais-e-passo-a-passo)
- [Cursos On-Line](#cursos-on-line)
- [Outros compilados](#outros-compilados)

---

## Artigos, Reportagens e Documentação

### Geral

- [Oracle apresenta supercomputador com 1060 Raspberry Pi na OOW](https://www.servethehome.com/oracle-shows-1060-raspberry-pi-supercomputer-at-oow/)
- [Duet Display agora está disponível também para Android](https://olhardigital.com.br/noticia/duet-display-agora-esta-disponivel-tambem-para-android/91056)
- [Como instalar o Ubuntu Touch no seu celular (Linux de bolso)](https://sempreupdate.com.br/como-instalar-o-ubuntu-touch-no-seu-celular-linux-de-bolso/)

### Linux

- [A Beginners Guide To Cron Jobs](https://www.ostechnix.com/a-beginners-guide-to-cron-jobs/)
- [28 fatos sobre o Linux em seu aniversário de 28 anos](https://www.redhat.com/en/blog/28-facts-about-linux-its-28th-birthday?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CvuUAAS)
- [Como mudar a cor do seu terminal Linux](https://opensource.com/article/19/9/linux-terminal-colors)
- [Como o Linux chegou ao mainframe](https://opensource.com/article/19/9/linux-mainframes-part-1?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CxUyAAK)
- [Linux no mainframe: então e agora](https://opensource.com/article/19/9/linux-mainframes-part-2)

### LPI Learning

- [LPI Learning](https://learning.lpi.org/en/)
- [Free Training Materials](https://learning.lpi.org/en/learning-materials/learning-materials/)

#### Essentials

- [Linux Essentials Oficial](https://learning.lpi.org/en/learning-materials/010-160/)
- [Curso Linux Essentials](https://lcnsqr.com/curso-linux-essentials)

#### LPIC-1

- [LPIC-1 Exam 101 Oficial](https://learning.lpi.org/en/learning-materials/101-500/)
- [A roadmap for LPIC-1](https://developer.ibm.com/tutorials/l-lpic1-map/)

### Segurança

- [SSH.COM](https://www.ssh.com/)
- [A cyber attack short story](https://blog.ssh.com/a-cyberattack-short-story)
- [Data breaches have a long-lasting impact](https://blog.ssh.com/breach_consequencies_are_long_lasting)

### Git

- [GIT - Documentação](https://git-scm.com/doc)
- [Manual | Primeiros passos](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Sobre-Controle-de-Versão)
- [Understanding the GitHub flow](https://guides.github.com/introduction/flow/)
- [Visual Git Cheat Sheet](http://ndpsoftware.com/git-cheatsheet.html)
- [13 dicas do Git para o 13º aniversário do Git](https://opensource.com/article/18/4/git-tips)
- [Gerencie sua agenda diária com o Git](https://opensource.com/article/19/4/calendar-git?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CrNZAA0)

### Markdown & Mermaid

- [Markdown User Guide](http://fletcher.github.io/peg-multimarkdown)
- [Tutorial Markdown](https://www.markdowntutorial.com)
- [Converta arquivos Markdown em documentos do processador de texto usando o pandoc](https://opensource.com/article/19/5/convert-markdown-to-word-pandoc)

### Ansible

- [10 módulos Ansible que você precisa conhecer](https://opensource.com/article/19/9/must-know-ansible-modules?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CxUyAAK)

### Hadoop

#### Sqoop & Importação de dados

- [Documentação: sqoop-import-mainframe](https://sqoop.apache.org/docs/1.4.7/SqoopUserGuide.html#_literal_sqoop_import_mainframe_literal)
- [Aprenda Sqoop Import Mainframe Tool - Sintaxe e Exemplos](https://data-flair.training/blogs/sqoop-import-mainframe/)
- [Descarregando dados de mainframe no Hadoop](https://community.cloudera.com/t5/Community-Articles/Offloading-Mainframe-Data-into-Hadoop/ta-p/246129)
- [Descarregando dados de mainframe no Hadoop: 4 coisas que você precisa saber](https://bigstep.com/blog/offloading-mainframe-data-hadoop-4-things-need-know)
- [Serde for Cobol Layout to Hive table](https://rbheemana.github.io/Cobol-to-Hive/)
- [Big Iron to Big Data: Mainframe to Hadoop with Apache Sqoop](https://blog.syncsort.com/2014/06/big-data/big-iron-big-data-mainframe-hadoop-apache-sqoop/)

#### Ranger

- [Práticas recomendadas na autorização de HDFS com o Apache Ranger](https://blog.cloudera.com/best-practices-in-hdfs-authorization-with-apache-ranger/)
- [Discussão sobre políticas do Ranger para HDFS](https://pierrevillard.com/2018/08/14/discussion-around-ranger-policies-for-hdfs/)
- [Umask - Wikipedia](https://pt.wikipedia.org/wiki/Umask)

#### HBase

- [Uma visão aprofundada da arquitetura do HBase](https://mapr.com/blog/in-depth-look-hbase-architecture/)
- [Diretrizes para o design do esquema HBase](https://mapr.com/blog/guidelines-hbase-schema-design/)
- [Banco de dados HBase e MapR: projetado para distribuição, escala e velocidade](https://mapr.com/blog/hbase-and-mapr-db-designed-distribution-scale-and-speed/)

### DevOps

- [Um guia para iniciantes na construção de pipelines DevOps com ferramentas de código aberto](https://opensource.com/article/19/4/devops-pipeline)

### Docker

- [Docker](https://docs.docker.com)
- [Docker Definitivo](https://dockerdefinitivo.com)

### Kubernetes

- [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

### Empresas

#### AWS

- [Webinars 2019 - AWS](https://aws.amazon.com/pt/webinars-2019/?&sc_channel=psm&sc_campaign=awswebinarondemandptb&sc_publisher=LI&sc_category=webinar&sc_country=BR&sc_geo=LATAM&sc_outcome=ln&sc_detail=800x400&sc_content=Webinar_On_Demand_PTB&sc_segment=pt_2&sc_medium=FIELD-P|LI|Social-P|All|LD|Webinar|Solution|BR|PT|Sponsored%20Content|xx|Webinar-on-Demand-PT)

#### HashiCorp

- [Hashicorp](https://learn.hashicorp.com)
- [Terraform](https://learn.hashicorp.com/terraform)
- [Instruqt](https://play.instruqt.com/hashicorp) (Estilo Katacoda)

#### IBM

- [IBM Coder Community](https://ibmcoders.influitive.com/challenges)

#### Mozilla

- [Mozilla Developer Network](https://developer.mozilla.org/pt-BR/)

### Sites

- [ServerTheHome](https://www.servethehome.com)
- [Linux Descomplicado](https://www.linuxdescomplicado.com.br)

### Ferramentas

- [VFsync - Virtual File Synchronization](https://vfsync.org/index.html)
- [Bitwarden - Cofre de Senhas](https://bitwarden.com)

---

## Tutoriais e Passo-a-Passo

- [Construindo pipelines de CI / CD com Jenkins](https://opensource.com/article/19/9/intro-building-cicd-pipelines-jenkins)
- [Criando uma aplicação com NodeJS, usando Express, Mongo, Docker e Babel](https://emersonbroga.com/passo-a-passo-criando-uma-aplicacao-com-nodejs-usando-express-mongo-docker-e-babel/)
- [Django On Kubernetes - o mais conciso possível](https://labs.meanpug.com/django-on-kubernetes-as-concisely-as-possible/)
- [Métricas de aplicativo personalizadas com Django, Prometheus e Kubernetes](https://labs.meanpug.com/custom-application-metrics-with-django-prometheus-and-kubernetes/)

---

## Cursos On-Line

- [WebFor - Curso AWS Certified Solutions Architect](https://webfor.com.br/curso-aws-certified-solutions-architect/)
- [MapR Academy](https://mapr.com/training/courses/)
- [Digital Innovation One](https://digitalinnovation.one/)
- [Instruqt](https://play.instruqt.com/)

---

## Outros compilados

- [Diego Pacheco - DevOps Learning the hard way](https://trello.com/b/ZFVZz4Cd/devops-learning-the-hard-way)
- [Andreas Kretz - The Data Engineering Cookbook](https://github.com/andkret/Cookbook)
